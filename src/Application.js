import * as mn from 'backbone.marionette';

var app = new mn.Application();

app.on('start', () => {
    console.log('Application start');
});

app.start();

export default app;