module.exports = function (grunt) {

    grunt.loadNpmTasks("grunt-webpack");

    grunt.initConfig({

        app: {
            src: 'src',
            target: 'target'
        },

        webpack: {
            options: {
                entry: './<%= app.src %>/Application.js',
                module: {
                    loaders: [
                        {
                            test: /\.js$/,
                            exclude: /node_modules/,
                            loader: 'babel-loader',
                            query: {
                                presets: ['es2015']
                            }
                        }
                    ]
                },
                resolve: {
                    modulesDirectories: ['node_modules', '<%= app.src %>']
                },
                output: {
                    path: './<%= app.target %>/',
                    filename: 'script.js',
                    pathinfo: true
                },
                cache: true,
                watch: true
            },
            build: {}
        }

    });

    grunt.registerTask('default', ['webpack']);

};